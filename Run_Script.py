from Exam_Core.Visualization import year_1996, year_1997
from Exam_Core.Statistics import North_Frisian, SouthEast_Frisian, SouthWest_Frisian, East_Groningen, Other_Groningen, \
    Delftzijl_and_Area


def visualize_1996():
    plot = year_1996.plot_1996()
    print("Barh plot of Most Common Cause of Death in different Areas in the Netherlands in 1996:")
    return plot


def visualize_1997():
    plot = year_1997.plot_1997()
    print("Barh plot of the Most Common Cause of Death in different Areas in the Netherlands in 1997:")
    return plot


def stats_north_frisian():
    stats = North_Frisian.one_way_anova()
    print("OneWay ANOVA North-Frisian between 1996 and 1997:")
    return stats


def stats_southeast_frisian():
    stats = SouthEast_Frisian.one_way_anova()
    print("OneWay ANOVA SouthEast-Frisian between 1996 and 1997:")
    return stats


def stats_southwest_frisian():
    stats = SouthWest_Frisian.one_way_anova()
    print("OneWay ANOVA SouthWest-Frisian between 1996 and 1997:")
    return stats


def stats_east_groningen():
    stats = East_Groningen.one_way_anova()
    print("OneWay ANOVA East-Groningen between 1996 and 1997:")
    return stats


def stats_delftzijl_and_area():
    stats = Delftzijl_and_Area.one_way_anova()
    print("OneWay ANOVA Delftzijl and Area between 1996 and 1997:")
    return stats


def stats_other_groningen():
    stats = Other_Groningen.one_way_anova()
    print("OneWay ANOVA Other-Groningen between 1996 and 1997:")
    return stats


def main():
    print(visualize_1996())
    print(visualize_1997())
    print(stats_north_frisian(), "There is NO significant difference between 1996 and 1997")
    print(stats_southeast_frisian(), "There is a significant difference between 1996 and 1997")
    print(stats_southwest_frisian(), "There is NO significant difference between 1996 and 1997")
    print(stats_east_groningen(), "There is NO significant difference between 1996 and 1997")
    print(stats_delftzijl_and_area(), "There is NO significant difference between 1996 and 1997")
    print(stats_other_groningen(), "There is NO significant difference between 1996 and 1997")


if __name__ == "__main__":
    print(main())
