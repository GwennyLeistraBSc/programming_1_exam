from Exam_Core.DataFrames_Per_Year.SouthEast_Frisian import SEF_1997
from Exam_Core.DataFrames_Per_Year.SouthEast_Frisian import SEF_1996
import pandas as pd
import scipy.stats as stats


def merge_sef():
    """
    Method to merge the DataFrames of SouthEast-Frisian of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(SEF_1996.sort_sef_1996(), SEF_1997.sort_sef_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of SouthEast-Frisian.
    :return: Result ANOVA.
    """
    df = merge_sef()
    anova = stats.f_oneway(df["SouthEast-Frisian_1996"], df["SouthEast-Frisian_1997"])
    return anova
