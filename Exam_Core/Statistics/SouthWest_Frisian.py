from Exam_Core.DataFrames_Per_Year.SouthWest_Frisian import SWF_1996, SWF_1997
import pandas as pd
import scipy.stats as stats


def merge_swf():
    """
    Method to merge the DataFrames of SouthWest-Frisian of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(SWF_1996.sort_swf_1996(), SWF_1997.sort_swf_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of SouthWest-Frisian.
    :return: Result ANOVA.
    """
    df = merge_swf()
    anova = stats.f_oneway(df["SouthWest-Frisian_1996"], df["SouthWest-Frisian_1997"])
    return anova
