from Exam_Core.DataFrames_Per_Year.Other_Groningen import OG_1997, OG_1996
import pandas as pd
import scipy.stats as stats


def merge_og():
    """
    Method to merge the DataFrames of Other_Groningen of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(OG_1996.sort_og_1996(), OG_1997.sort_og_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of Other_Groningen.
    :return: Result ANOVA.
    """
    df = merge_og()
    anova = stats.f_oneway(df["Other Groningen_1996"], df["Other Groningen_1997"])
    return anova
