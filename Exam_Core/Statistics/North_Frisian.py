from Exam_Core.DataFrames_Per_Year.North_Frisian import NF_1996
from Exam_Core.DataFrames_Per_Year.North_Frisian import NF_1997
import pandas as pd
import scipy.stats as stats


def merge_nf():
    """
    Method to merge the DataFrames of North-Frisian of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(NF_1996.sort_nf_1996(), NF_1997.sort_nf_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of North-Frisian.
    :return: Result ANOVA.
    """
    df = merge_nf()
    anova = stats.f_oneway(df["North-Frisian_1996"], df["North-Frisian_1997"])
    return anova
