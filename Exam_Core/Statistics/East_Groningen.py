from Exam_Core.DataFrames_Per_Year.East_Groningen import EG_1996
from Exam_Core.DataFrames_Per_Year.East_Groningen import EG_1997
import pandas as pd
import scipy.stats as stats


def merge_eg():
    """
    Method to merge the DataFrames of East_Groningen of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(EG_1996.sort_eg_1996(), EG_1997.sort_eg_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of East_Groningen.
    :return: Result ANOVA.
    """
    df = merge_eg()
    anova = stats.f_oneway(df["East-Groningen_1996"], df["East-Groningen_1997"])
    return anova
