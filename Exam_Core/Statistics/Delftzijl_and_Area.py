from Exam_Core.DataFrames_Per_Year.Delftzijl_and_Area import DAA_1996, DAA_1997
import pandas as pd
import scipy.stats as stats


def merge_daa():
    """
    Method to merge the DataFrames of Delftzijl and Area of all years, category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(DAA_1996.sort_daa_1996(), DAA_1997.sort_daa_1997(), left_index=True, right_index=True,
                  suffixes=("_1996", "_1997"))
    return df


def one_way_anova():
    """
    Method to perform the one-way ANOVA test on the merged DataFrame of Delftzijl and Area.
    :return: Result ANOVA.
    """
    df = merge_daa()
    anova = stats.f_oneway(df["Delftzijl and Area_1996"], df["Delftzijl and Area_1997"])
    return anova
