import numpy as np
from Exam_Core.Translate import column_header, column_age, column_area, column_gender
from Exam_Core.Read_Files.read_groningen_file import read_groningen_file
from Exam_Core.Read_Files.file_path import file_path


def translate_header():
    """
    Method to translate the column headers of the DataFrame.
    :return: Pandas DataFrame.
    """
    df = read_groningen_file()
    df = column_header.column_header(df)
    return df


def translate_age():
    """
    Method to translate the values of the Column Age in the DataFrame.
    :return: Pandas DataFrame.
    """
    df = translate_header()
    df = column_age.column_age(df)
    return df


def translate_area():
    """
    Method to translate the values of the Column Area in the DataFrame.
    :return: Pandas DataFrame.
    """
    df = translate_age()
    df = column_area.column_area(df)
    return df


def translate_gender():
    """
    Method to translate the values of the Column Gender in the DataFrame.
    :return: Pandas DataFrame.
    """
    df = translate_area()
    df = column_gender.column_gender(df)
    return df


def tune_year():
    """
    Method to remove the last 4 (four) letters of the values in the column Year of the DataFrame.
    :return: Pandas DataFrame.
    """
    df = translate_gender()
    df["Year"] = df["Year"].astype(str).str[:-4].astype(np.int64)
    return df


def remove_unnecessary_columns():
    """
    Method to remove unnecessary columns in the DataFrame.
    :return: Pandas DataFrame.
    """
    df = tune_year()
    remove = "Total"
    for column in df:
        if remove in column:
            del df[column]
    del df["ID"]
    return df


def get_df_groningen():
    """
    Method to get the fully translated Groningen DataFrame.
    :return: Pandas DataFrame.
    """
    df = remove_unnecessary_columns()
    return df



