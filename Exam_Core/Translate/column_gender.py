
def column_gender(df):
    """
    Method to translate the values in the column Gender of a DataFrame.
    :param df: Pandas DataFrame.
    :return: Pandas DataFrame.
    """
    gender = {1100: "All",
              3000: "Men",
              4000: "Woman"
              }
    df["Gender"] = df["Gender"].map(gender)
    return df
