
def column_area(df):
    """
    Method to translate the values in the column Area of a DataFrame.
    :param df: Pandas DataFrame.
    :return: Pandas DataFrame.
    """
    area = {"PV20  ": "Groningen (Province)",
            "PV21  ": "Frisia (Province)",
            "GM0070": "Franekeradeel",
            "CR01  ": "East-Groningen",
            "CR02  ": "Delftzijl and Area",
            "CR03  ": "Other Groningen",
            "CR04  ": "North-Frisian",
            "CR05  ": "SouthWest-Frisian",
            "CR06  ": "SouthEast-Frisian",
            }
    df["Area"] = df["Area"].map(area)
    return df
