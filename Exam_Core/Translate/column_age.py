
def column_age(df):
    """
    Method to translate the values in the column Age of a DataFrame.
    :param df: Pandas DataFrame
    :return: Pandas DataFrame
    """
    age = {10000: "All",
           21900: "above_90",
           41650: "0-50",
           60600: "50-60",
           71300: "60-65",
           71400: "65-70",
           71500: "70-75",
           71600: "75-80",
           71700: "80-85",
           71800: "85-90",
           }
    df["Age"] = df["Age"].map(age)
    return df
