from pathlib import Path


def file_path():
    """
    Method to get the filepath to the Data directory.
    Change the /Users/Gwenny/Pycharm with own path, and leave programming_1_exam/Data/file_name the way they are.
    :return: string.
    """
    file_path = Path(__file__).parent.parent
    return str(file_path)
