import pandas as pd
from Exam_Core.Read_Files.file_path import file_path


def read_friesland_file():
    """
    Method to read the csv.file.
    :return: Pandas DataFrame.
    """
    path = file_path()
    path = path + "/Data/CauseOfDeath_Friesland.csv"
    df = pd.read_csv(path, sep=";")
    return df
