from Exam_Core.Translate.Translate_DF_Friesland import get_df_friesland
from Exam_Core.DataFrames_Per_Area.Template_DataFrames_per_Area import get_area, get_gender, get_age


def get_southeastfrisian():
    """
    Method to create a DataFrame of SouthEast-Frisian.
    :return: Pandas DataFrame.
    """
    df = get_area("SouthEast-Frisian", get_df_friesland())
    return df


def get_sef_all_age():
    """
    Method to create a DataFrame of SouthEast-Frisian with Category: Age: All.
    :return: Pandas DataFrame.
    """
    df = get_age("All", get_southeastfrisian())
    return df


def get_sef_all_age_gender():
    """
    Method to create a DataFrame of SouthEast-Frisian with Category: Age: All.
    :return: Pandas DataFrame.
    """
    df = get_gender("All", get_sef_all_age())
    return df
