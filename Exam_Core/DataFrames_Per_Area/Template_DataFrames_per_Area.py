
def get_area(area, function_df):
    """
    Method to create a DataFrame of a specific Area.
    :param area: String.
    :param function_df: Function.
    :return: Pandas DataFrame.
    """
    df = function_df
    df = df[df.Area == area]
    df = df.set_index("Area")
    return df


def get_age(age, function_df):
    """
    Method to create a DataFrame of a specific Age.
    :param age: String.
    :param function_df: Function.
    :return: Pandas DataFrame.
    """
    df = function_df
    df = df[df.Age == age]
    return df


def get_gender(gender, function_df):
    """
    Method to create a DataFrame of a specific gender.
    :param gender: String.
    :param function_df: Function.
    :return: Pandas DataFrame.
    """
    df = function_df
    df = df[df.Gender == gender]
    return df
