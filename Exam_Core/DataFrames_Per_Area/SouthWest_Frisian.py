from Exam_Core.Translate.Translate_DF_Friesland import get_df_friesland
from Exam_Core.DataFrames_Per_Area.Template_DataFrames_per_Area import get_age, get_area, get_gender


def get_southwestfrisian():
    """
    Method to create a DataFrame of SouthWest-Frisian.
    :return: Pandas DataFrame.
    """
    df = get_area("SouthWest-Frisian", get_df_friesland())
    return df


def get_swf_all_age():
    """
    Method to create a DataFrame of SouthWest_Frisian with category: Age: All.
    :return: Pandas DataFrame.
    """
    df = get_age("All", get_southwestfrisian())
    return df


def get_swf_all_age_gender():
    """
    Method to create a DataFrame of SouthWest_Frisian with category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = get_gender("All", get_swf_all_age())
    return df
