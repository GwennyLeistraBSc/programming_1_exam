from Exam_Core.Translate.Translate_DF_Groningen import get_df_groningen
from Exam_Core.DataFrames_Per_Area.Template_DataFrames_per_Area import get_gender, get_age, get_area


def get_other_groningen():
    """
    Method to create a DataFrame of Other-Groningen.
    :return: Pandas DataFrame.
    """
    df = get_area("Other Groningen", get_df_groningen())
    return df


def get_og_all_age():
    """
    Method to create a DataFrame of Other-Groningen with Age category: All
    :return: Pandas DataFrame.
    """
    df = get_age("All", get_other_groningen())
    return df


def get_og_all_age_gender():
    """
    Method to create a DataFrame of Other-Groningen with age category: All, Gender category: All.
    :return: Pandas DataFrame.
    """
    df = get_gender("All", get_og_all_age())
    return df
