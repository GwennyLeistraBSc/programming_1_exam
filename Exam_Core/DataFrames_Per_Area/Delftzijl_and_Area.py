from Exam_Core.Translate.Translate_DF_Groningen import get_df_groningen
from Exam_Core.DataFrames_Per_Area.Template_DataFrames_per_Area import get_gender, get_age, get_area


def get_delftzijl_and_area():
    """
    Method to create a DataFrame of Delftzijl and Area.
    :return: Pandas DataFrame.
    """
    df = get_area("Delftzijl and Area", get_df_groningen())
    return df


def get_daa_all_age():
    """
    Method to create a DataFrame of Delftzijl and Area with Age category: All
    :return: Pandas DataFrame.
    """
    df = get_age("All", get_delftzijl_and_area())
    return df


def get_daa_all_age_gender():
    """
    Method to create a DataFrame of Delftzijl and Area with age category: All, Gender category: All.
    :return: Pandas DataFrame.
    """
    df = get_gender("All", get_daa_all_age())
    return df
