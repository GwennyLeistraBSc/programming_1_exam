from Exam_Core.Translate.Translate_DF_Friesland import get_df_friesland
from Exam_Core.DataFrames_Per_Area.Template_DataFrames_per_Area import get_gender, get_age, get_area


def get_northfrisian():
    """
    Method to create a DataFrame of North_Frisian.
    :return: Pandas DataFrame.
    """
    df = get_area("North-Frisian", get_df_friesland())
    return df


def get_nf_all_age():
    """
    Method to create a DataFrame of North_Frisian with Age category: All
    :return: Pandas DataFrame.
    """
    df = get_age("All", get_northfrisian())
    return df


def get_nf_all_age_gender():
    """
    Method to create a DataFrame of North_Frisian with age category: All, Gender category: All.
    :return: Pandas DataFrame.
    """
    df = get_gender("All", get_nf_all_age())
    return df
