from Exam_Core.DataFrames_Per_Area.Delftzijl_and_Area import get_daa_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, \
    get_transpose, get_sort


def get_daa_1996():
    """
    Method to create a DataFrame of Delftzijl and Area, with category: Age: All, Gender: All, Year: 1996
    :return: Pandas DataFrame.
    """
    df = get_year(1996, get_daa_all_age_gender())
    return df


def transpose_daa_1996():
    """
    Method to transpose the Delftzijl and Area DataFrame with category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_daa_1996())
    return df


def sort_daa_1996():
    """
    Method to sort the values, ascending: False of the Delftzijl and Area DataFrame
    with Category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_daa_1996(), "1996", "Delftzijl and Area")
    return df
