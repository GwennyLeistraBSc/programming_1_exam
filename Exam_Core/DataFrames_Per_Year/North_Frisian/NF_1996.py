from Exam_Core.DataFrames_Per_Area.North_Frisian import get_nf_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, \
    get_transpose, get_sort


def get_nf_1996():
    """
    Method to create a DataFrame of North_Frisian, with category: Age: All, Gender: All, Year: 1996
    :return: Pandas DataFrame.
    """
    df = get_year(1996, get_nf_all_age_gender())
    return df


def transpose_nf_1996():
    """
    Method to transpose the North-Frisian DataFrame with category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_nf_1996())
    return df


def sort_nf_1996():
    """
    Method to sort the values, ascending: False of the North-Frisian DataFrame
    with Category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_nf_1996(), "1996", "North-Frisian")
    return df

