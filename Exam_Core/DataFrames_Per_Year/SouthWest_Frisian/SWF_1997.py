from Exam_Core.DataFrames_Per_Area.SouthWest_Frisian import get_swf_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, \
    get_transpose, get_sort


def get_swf_1997():
    """
    Method to create a DataFrame of SouthWest-Frisian with category:
    Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_year(1997, get_swf_all_age_gender())
    return df


def transpose_swf_1997():
    """
    Method to transpose a DataFrame of SouthWest-Frisian with category:
    Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_swf_1997())
    return df


def sort_swf_1997():
    """
    Method to sort the values, ascending: False of the SouthWest-Frisian DataFrame
    with Category: Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_swf_1997(), "1997", "SouthWest-Frisian")
    return df
