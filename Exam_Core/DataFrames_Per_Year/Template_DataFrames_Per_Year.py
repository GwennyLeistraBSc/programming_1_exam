
def get_year(year, df_function):
    """
    Method to create a DataFrame of a specific year.
    :param year: Number
    :param df_function: Function.
    :return: Pandas DataFrame.
    """
    df = df_function
    df = df[df.Year == year]
    return df


def get_transpose(df_function):
    """
    Method to transpose a DataFrame.
    :return: Pandas DataFrame.
    """
    df = df_function
    df = df.transpose()
    df = df.drop("Gender")
    df = df.drop("Age")
    df = df.drop("Year")
    return df


def get_sort(df_function, df_year, sort_by):
    """
    Method to sort the values of a DataFrame.
    :param df_function: Function.
    :param df_year: String.
    :param sort_by: String.
    :return: Pandas DataFrame.
    """
    df = df_function
    df.index.name = df_year
    df = df.sort_values(by=sort_by, ascending=False)
    return df

