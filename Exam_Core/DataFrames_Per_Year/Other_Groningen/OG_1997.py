from Exam_Core.DataFrames_Per_Area.Other_Groningen import get_og_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, \
    get_transpose, get_sort


def get_og_1997():
    """
    Method to create a DataFrame of Other-Groningen, with category: Age: All, Gender: All, Year: 1997
    :return: Pandas DataFrame.
    """
    df = get_year(1997, get_og_all_age_gender())
    return df


def transpose_og_1997():
    """
    Method to transpose the Other-Groningen DataFrame with category: Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_og_1997())
    return df


def sort_og_1997():
    """
    Method to sort the values, ascending: False of the Other-Groningen DataFrame
    with Category: Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_og_1997(), "1997", "Other Groningen")
    return df
