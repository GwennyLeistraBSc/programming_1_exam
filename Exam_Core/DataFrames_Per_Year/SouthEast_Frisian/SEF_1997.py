from Exam_Core.DataFrames_Per_Area.SouthEast_Frisian import get_sef_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, get_transpose, \
    get_sort


def get_sef_1997():
    """
    Method to create a DataFrame of SouthEast-Frisian with category:
    Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_year(1997, get_sef_all_age_gender())
    return df


def transpose_sef_1997():
    """
    Method to transpose a DataFrame of SouthEast-Frisian with category:
    Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_sef_1997())
    return df


def sort_sef_1997():
    """
    Method to sort the values, ascending: False of the SouthEast-Frisian DataFrame
    with Category: Age: All, Gender: All, Year: 1997.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_sef_1997(), "1997", "SouthEast-Frisian")
    return df

