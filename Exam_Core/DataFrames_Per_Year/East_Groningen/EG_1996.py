from Exam_Core.DataFrames_Per_Area.East_Groningen import get_eg_all_age_gender
from Exam_Core.DataFrames_Per_Year.Template_DataFrames_Per_Year import get_year, \
    get_transpose, get_sort


def get_eg_1996():
    """
    Method to create a DataFrame of East-Groningen, with category: Age: All, Gender: All, Year: 1996
    :return: Pandas DataFrame.
    """
    df = get_year(1996, get_eg_all_age_gender())
    return df


def transpose_eg_1996():
    """
    Method to transpose the East-Groningen DataFrame with category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_transpose(get_eg_1996())
    return df


def sort_eg_1996():
    """
    Method to sort the values, ascending: False of the East-Groningen DataFrame
    with Category: Age: All, Gender: All, Year: 1996.
    :return: Pandas DataFrame.
    """
    df = get_sort(transpose_eg_1996(), "1996", "East-Groningen")
    return df

