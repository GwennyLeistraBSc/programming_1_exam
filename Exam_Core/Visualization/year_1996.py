from Exam_Core.DataFrames_Per_Year.North_Frisian import NF_1996
from Exam_Core.DataFrames_Per_Year.SouthEast_Frisian import SEF_1996
from Exam_Core.DataFrames_Per_Year.SouthWest_Frisian import SWF_1996
from Exam_Core.DataFrames_Per_Year.East_Groningen import EG_1996
from Exam_Core.DataFrames_Per_Year.Delftzijl_and_Area import DAA_1996
from Exam_Core.DataFrames_Per_Year.Other_Groningen import OG_1996
import matplotlib.pyplot as plt
import pandas as pd


NF_1996 = NF_1996.sort_nf_1996().head()
SEF_1996 = SEF_1996.sort_sef_1996().head()
SWF_1996 = SWF_1996.sort_swf_1996().head()
EG_1996 = EG_1996.sort_eg_1996().head()
DAA_1996 = DAA_1996.sort_daa_1996().head()
OG_1996 = OG_1996.sort_og_1996().head()


def merge_df_1996():
    """
    Method to merge all DataFrames of 1996, with category: Age: All, Gender: All.
    :return: Pandas DataFrame.
    """
    df = pd.merge(NF_1996, SEF_1996, left_index=True, right_index=True)
    df = pd.merge(df, SWF_1996, left_index=True, right_index=True)
    df = pd.merge(df, EG_1996, left_index=True, right_index=True)
    df = pd.merge(df, DAA_1996, left_index=True, right_index=True)
    df = pd.merge(df, OG_1996, left_index=True, right_index=True)
    return df


def plot_1996():
    """
    Method to plot the DataFrame of 1996. Type = Barh.
    :return: nothing.
    """
    df = merge_df_1996()
    ax = df.plot(kind="barh", title="Most common Cause of Death 1996 per Area in the Netherlands.")
    ax.set_ylabel("Cause of Death.")
    ax.set_xlabel("Number of Deaths (Individuals).")
    plt.setp(ax.get_yticklabels(), ha="right", rotation=45)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), shadow=True)
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.show()


#merge_df_1996()
#plot_1996()
