from Exam_Core.DataFrames_Per_Year.North_Frisian import NF_1997
from Exam_Core.DataFrames_Per_Year.SouthEast_Frisian import SEF_1997
from Exam_Core.DataFrames_Per_Year.SouthWest_Frisian import SWF_1997
from Exam_Core.DataFrames_Per_Year.East_Groningen import EG_1997
from Exam_Core.DataFrames_Per_Year.Delftzijl_and_Area import DAA_1997
from Exam_Core.DataFrames_Per_Year.Other_Groningen import OG_1997
import matplotlib.pyplot as plt
import pandas as pd


NF_1997= NF_1997.sort_nf_1997().head()
SEF_1997 = SEF_1997.sort_sef_1997().head()
SWF_1997 = SWF_1997.sort_swf_1997().head()
EG_1997 = EG_1997.sort_eg_1997().head()
DAA_1997 = DAA_1997.sort_daa_1997().head()
OG_1997 = OG_1997.sort_og_1997().head()


def merge_df_1997():
    """
    Method to merge all DataFrames of 1997 with category: Age: All, Gender: All..
    :return: Pandas DataFrame.
    """
    df = pd.merge(NF_1997, SEF_1997, left_index=True, right_index=True)
    df = pd.merge(df, SWF_1997, left_index=True, right_index=True)
    df = pd.merge(df, EG_1997, left_index=True, right_index=True)
    df = pd.merge(df, DAA_1997, left_index=True, right_index=True)
    df = pd.merge(df, OG_1997, left_index=True, right_index=True)
    return df


def plot_1997():
    """
    Method to plot the DataFrame of 1997. Type = Barh.
    :return: Nothing.
    """
    df = merge_df_1997()
    ax = df.plot(kind="barh", title="Most common Cause of Death 1997 per Area in the Netherlands.")
    ax.set_ylabel("Cause of Death.")
    ax.set_xlabel("Number of Deaths (Individuals).")
    plt.setp(ax.get_yticklabels(), ha="right", rotation=45)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), shadow=True)
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.show()


#merge_df_1997()
#plot_1997()
