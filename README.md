#Programming_1_Exam
##Version 1.0
###Author: Gwenny Leistra, BSc 
####Studentnumber: 340314

Python program to visualize the most common cause of death
in different areas in the Netherlands, current added:
- North-Frisian, year: 1996, 1997.
- SouthEast-Frisian, year: 1996, 1997.
- SouthWest-Frisian, year: 1996, 1997.
- East-Groningen, year: 1996, 1997.
- Delftzijl and Area, year: 1996, 1997.
- Other Groningen, year: 1996, 1997.

For the above areas an OneWay ANOVA was performed to 
analyse if there is a significant difference in cause of death between
the years. 

#Install
- Terminal: git clone https://GwennyLeistraBSc@bitbucket.org/GwennyLeistraBSc/programming_1_exam.git
- NONTerminal: Go to --> https://bitbucket.org/GwennyLeistraBSc/programming_1_exam/src/master/

#Usage
- For running the program, visualization and OneWay ANOVA:
- Window: py -3 Run_Script.py
- UNIX: python3 Run_Script.py
 

- For the Unit-test run:
- Windows: py -3 Unit_Test.py
- UNIX: python3 Unit_Test.py

#Dependencies
- Pandas version: v0.24.2
- matplotlib version: v3.1.0
- To install the dependecies run: pip install "package", same for window and UNIX.


And at least! Have FUN! :) 

