import unittest
import pandas as pd
from Exam_Core.Read_Files import read_friesland_file, read_groningen_file


class ReadFileTest(unittest.TestCase):
    def setUp(self):
        """
        Method to store the self variables for the Unit_Testing.
        :return: Nothing.
        """
        self.data = pd.DataFrame
        self.read_friesland = read_friesland_file
        self.read_groningen = read_groningen_file
        self.incorrect_data = pd.Series

    def test_read(self):
        """
        Method to test if the read_friesland and read_groningen_file() returns a pandas DataFrame.
        :return: Nothing.
        """
        self.assertIsInstance(self.read_friesland.read_friesland_file(), self.data)
        self.assertIsInstance(self.read_groningen.read_groningen_file(), self.data)

    def test_incorrect_read(self):
        """
        Method to test if the read_friesland and read_groningen_file() does not return a pandas Series.
        :return: Nothing.
        """
        self.assertNotIsInstance(self.read_friesland.read_friesland_file(), self.incorrect_data)
        self.assertNotIsInstance(self.read_groningen.read_groningen_file(), self.incorrect_data)


if __name__ == "__main__":
    unittest.main()
